/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces.reports;

import java.util.Map;

/**
 *
 * @author marcin
 */
public interface IRaportChanged {
    /**
     * Have to be invoked when raport changes
     */
    public void raportChanged();
    /**
     * Have to be invoked when KPI is recalculated
     * @param newValues New values of KPI
     */
    public void setKpis(Map<String,Double> newValues);
}
