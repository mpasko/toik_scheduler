/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces.gui;

/**
 * Exception that indicates, that threshold setting has wrong or missing operator
 * @author marcin
 */
public class InvalidOperatorException extends Exception {

    public InvalidOperatorException(String string) {
        super(string);
    }
    
}
