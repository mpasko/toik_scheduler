/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces.gui;

import java.util.List;

/**
 * Main interface to access Scheduler configuration
 * @author marcin
 */
public interface ISchedulerProxy {
    
    /**
     * Gets time intervals when some list of users should be notified periodically. 
     * Takes list of separate definitions each in cron format.
     * @param raportID The id of the report
     * @param timeStart Schedule configurations in cron format
     */
    List<String> getScheduleTime(String raportID);
    
    /**
     * Gets the list of users that should be notified in configured time periods
     * @param raportID The id of the report
     * @param users list of users 
     */
    List<? extends IUser> getUsersToSendPeriodic(String raportID);
    
    /**
     * Sets the list of users that should be notified if an KPI exceeds configured threshold
     * @param raportID The id of the report
     * @param users list of users
     */
    List<? extends IUser> getUsersToNotify(String raportID);
    
    /**
     * Sets the settings of the thresholds in format KpiName[>|<|>=|<=|=]value. In example:
     * OverallPerformance>=4.5
     * @param raportID The id of the report
     * @param thresholds
     * @throws Exception 
     */
    List<String> getThresholdSettings(String raportID);
    
    /**
     * Gets the list of currently fetched all available KPI's names from Report component
     * @param raportID
     * @param kpiList 
     */
    List<String> getKPIList(String raportID);
    
    /**
     * Sets the message that will be sent when threshold exceeds
     * @param raportID The id of the report
     * @param message Message that is to be send to the user
     */
    String getMessageWhenThreshold(String raportID);
    
    /**
     * Sets the message, that scheduler will be sending in scheduled periods
     * @param raportID The id of the raport
     * @param message Message that is to be send to the user
     */
    String getMessageWhenPlanned(String raportID);
    
    /**
     * Sets time intervals when some list of users should be notified periodically. 
     * Takes list of separate definitions each in cron format.
     * @param raportID The id of the report
     * @param timeStart Schedule configurations in cron format
     */
    void setScheduleTime(String raportID, List<String> timeStart);
    
    /**
     * Sets the list of users that should be notified in configured time periods
     * @param raportID The id of the report
     * @param users list of users 
     */
    void setUsersToSendPeriodic(String raportID, List<? extends IUser> users);
    
    /**
     * Sets the list of users that should be notified if an KPI exceeds configured threshold
     * @param raportID The id of the report
     * @param users list of users
     */
    void setUsersToNotify(String raportID, List<? extends IUser> users);
    
    /**
     * Sets the settings of the thresholds in format KpiName[>|<|>=|<=|=]value. In example:
     * OverallPerformance>=4.5
     * @param raportID The id of the report
     * @param thresholds
     * @throws Exception 
     */
    void setThresholdSettings(String raportID, List<String> thresholds) throws Exception;
    
    /**
     * Deprecated for GUI
     * @param raportID
     * @param kpiList 
     */
    void setKPIList(String raportID, List<String> kpiList);
    
    /**
     * Sets the message that will be sent when threshold exceeds
     * @param raportID The id of the report
     * @param message Message that is to be send to the user
     */
    void setMessageWhenThreshold(String raportID, String message);

    /**
     * Sets the message, that scheduler will be sending in scheduled periods
     * @param raportID The id of the raport
     * @param message Message that is to be send to the user
     */
    void setMessageWhenPlanned(String raportID, String message);
    
    /**
     * Registers listener for GUI Refresh functionality
     * @param raportID
     * @param listener 
     */
    void registerListener(String raportID, IGuiRefreshListener listener);
    
    /**
     * Unregisters listener found by raportID
     * @param raportID
     */
    void unregisterListener(String raportID);
    
    /**
     * Unregisters listener found by listener instance
     * @param listener 
     */
    void unregisterListener(IGuiRefreshListener listener);
}
