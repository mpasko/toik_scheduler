/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces.gui;

/**
 * Listener interface for GUI Refreshing functionality
 * @author marcin
 */
public interface IGuiRefreshListener {
    /**
     * Invoked when refresh GUI operation is to be performed
     * @param raportId -desired raport ID
     */
    void refreshGui(String raportId);
}
