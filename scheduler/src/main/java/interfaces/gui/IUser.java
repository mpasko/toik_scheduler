/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces.gui;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * Provides information about user
 * @author marcin
 */
public interface IUser {
    /**
     * Optional user name
     * @return 
     */
    public String getName();
    /**
     * Optional user name
     * @param name 
     */
    public void setName(String name);
    
    /**
     * User email address
     * @return email as string
     */
    public String getEmail();
    
    /**
     * Sets user email address
     * @param email address as string
     */
    public void setEmail(String email);
}
