/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces.gui;

/**
 * Exception that indicates, that threshold setting has wrong or missing KPI name or numerical value
 * @author marcin
 */
public class ExpectedKpiAndValueException extends Exception {

    public ExpectedKpiAndValueException(String string) {
        super(string);
    }
    
}
