package Camel;

import model.Planner;
import model.User;

import org.apache.camel.Exchange;
import org.apache.camel.Predicate;
import org.apache.camel.builder.RouteBuilder;

public class SchedulerRouteBuilder extends RouteBuilder{
	
    private double threshold;
	private String cron;
	private String recipients;
	private String id;
	private String notifyTo;
	private double kpi;
	private String file = "target/subfolder/";
	
	public void setCron(String cron){
		this.cron = "cron=" + cron;
	}
	
	public String getCron(){
		return cron;
	}
    
    public void setThreshold(int threshold){
    	this.threshold = threshold;
    }
    
    public double getThreshold(){
    	return this.threshold;
    }
	
	public SchedulerRouteBuilder(Planner planner){
		//Creating new route with parameters from planner
        /* TODO -manage remaining elements of the list, not single first! */
		this.cron = "cron=" + planner.getScheduleTime().get(0);
		StringBuilder sb = new StringBuilder();
		sb.append("&To=");
		for(User u : planner.getMailtoPeriodic()){
			sb.append(u.getEmail());
		}
		recipients = sb.toString();
		StringBuilder notifyBuilder = new StringBuilder();
		notifyBuilder.append("&To=");
		for(User u : planner.getMailto()){
			notifyBuilder.append(u.getEmail());
		}
		notifyTo = notifyBuilder.toString();
		this.id = planner.getRaportID();
		this.threshold = planner.getThresholds().get(0).getValue();
		this.kpi = Double.parseDouble(planner.getThresholds().get(0).kpi);

	}
	@Override
	public void configure() throws Exception {
		AddAttachmentProcessor addAttachment = new AddAttachmentProcessor(file, id);
		
		//Checking if kpi is lower than threshold
		Predicate notifyPred = new Predicate(){
			@Override
			public boolean matches(Exchange exchange) {
				log.info("Kpi on notifyPred:" + kpi );
				if (kpi < threshold){
					return true;
				}
				else return false;
			}
			
		};

		//scheduler route
		from("quartz://myTimerName"+ id + "?" + cron)
			.to("direct:sendPeriodic" + id)
			.choice()
				.when(notifyPred).to("direct:sendNotification" + id)
			.otherwise()
				.log("Threshold ok")
		.end();
			
		//Sending notification, becouse kpi is low
		from("direct:sendNotification" + id)
		    .process(new NotifyProcessor(file, id))
			.log("Sending notification, kpi low")
		    .setHeader("subject", simple("Low KPI"))
		    .to("smtps://smtp.gmail.com:465?password=scheduler11&username=toikscheduler" + notifyTo); 
		
		
		//Sending periodic raport
		from("direct:sendPeriodic" + id)
			.log("Working on file ${header.CamelFileName}")
		    .setHeader("subject", simple("New incident: ${header.CamelFileName}"))
		    .process(addAttachment)
		    .to("smtps://smtp.gmail.com:465?password=scheduler11&username=toikscheduler" + recipients); 

    
		
	}
	
}