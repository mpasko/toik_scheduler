package Camel;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;

public class AddAttachmentProcessor implements Processor{
	private String attachmentDir;
	private String id;
	
	public AddAttachmentProcessor(String dir, String id){

		this.attachmentDir = dir;
		this.id = id;
	}

	@Override
	public void process(Exchange exchange) throws Exception {
	
        Message in = exchange.getIn();
        
        in.setBody("Hello");

        String fileName = id;
        String absolutePath = this.attachmentDir + fileName;

        in.addAttachment(fileName, new DataHandler(new FileDataSource(absolutePath)));
	}
	
}
