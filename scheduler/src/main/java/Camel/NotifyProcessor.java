package Camel;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;

public class NotifyProcessor implements Processor{
	private String attachmentDir;
	private String id;
	
	public NotifyProcessor(String dir, String id){
		this.attachmentDir = dir;
		this.id = id;
	}

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		Message in = exchange.getIn();
		in.setBody("Low kpi in raport: " + id);
		String fileName = id;
		String absolutePath = this.attachmentDir + fileName;
        in.removeAttachment(fileName);
	}
	
}

