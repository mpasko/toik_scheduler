/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package scheduler;

import interfaces.gui.IGuiRefreshListener;
import interfaces.gui.ISchedulerProxy;
import interfaces.gui.IUser;
import java.util.LinkedList;
import java.util.List;

import org.apache.camel.CamelContext;
import org.apache.camel.impl.DefaultCamelContext;

import model.Planner;
import model.User;
import services.DateConverter;
import services.FilePersistence;
import services.SessionContainerFactory;
import services.ThresholdParser;

/**
 *
 * @author marcin
 */
class Scheduler implements ISchedulerProxy {
    private FilePersistence persistence;
    
    public List<String> allKPIs = new LinkedList<>();
    private String basePath;
    private CamelContext camelContext;
    
    public CamelContext getCamelContext(){
    	return camelContext;
    }
    
    public Scheduler() {
        basePath = "raports/";
        this.persistence = new FilePersistence(basePath);
        this.camelContext = new DefaultCamelContext();
    }
    
    private Planner getPlanner(String raportId){
        return persistence.load(String.format("raport%s.xml", raportId));
    }
    
    private void setPlanner(String raportId, Planner planner){
        persistence.save(String.format("raport%s.xml", raportId), planner);
    }

    @Override
    public List<? extends IUser> getUsersToNotify(String raportID) {
        return getPlanner(raportID).getMailto();
    }

    @Override
    public List<String> getThresholdSettings(String raportID) {
        return ThresholdParser.thresholdsToString(getPlanner(raportID).getThresholds());
    }

    @Override
    public List<String> getKPIList(String raportID) {
        return this.allKPIs;
    }

    @Override
    public void setUsersToNotify(String raportID, List<? extends IUser> users) {
        Planner planner = getPlanner(raportID);
        planner.setMailto(concretizeUsers(users));
        setPlanner(raportID, planner);
    }

    @Override
    public void setThresholdSettings(String raportID, List<String> thresholds) throws Exception{
        Planner planner = getPlanner(raportID);
        planner.setThresholds(ThresholdParser.stringsToThresholds(thresholds));
        setPlanner(raportID, planner);
    }

    @Override
    public void setKPIList(String raportID, List<String> kpiList) {
        this.allKPIs = kpiList;
    }

    @Override
    public void setScheduleTime(String raportID, List<String> time) {
        Planner planner = getPlanner(raportID);
        planner.setScheduleTime(DateConverter.scheduleUserToCamel(time));
        setPlanner(raportID, planner);
    }

    @Override
    public List<? extends IUser> getUsersToSendPeriodic(String raportID) {
        return getPlanner(raportID).getMailtoPeriodic();
    }

    @Override
    public void setUsersToSendPeriodic(String raportID, List<? extends IUser> users) {
        Planner planner = getPlanner(raportID);
        planner.setMailtoPeriodic(concretizeUsers(users));
        setPlanner(raportID, planner);
    }
    
    @Override
    public List<String> getScheduleTime(String raportID) {
        return DateConverter.scheduleCamelToUser(getPlanner(raportID).getScheduleTime());
    }

    @Override
    public String getMessageWhenThreshold(String raportID) {
        return getPlanner(raportID).getMessageThresholded();
    }

    @Override
    public String getMessageWhenPlanned(String raportID) {
        return getPlanner(raportID).getMessagePeriodic();
    }

    @Override
    public void setMessageWhenThreshold(String raportID, String message) {
        Planner planner = getPlanner(raportID);
        planner.setMessageThresholded(message);
        setPlanner(raportID, planner);
    }

    @Override
    public void setMessageWhenPlanned(String raportID, String message) {
        Planner planner = getPlanner(raportID);
        planner.setMessagePeriodic(message);
        setPlanner(raportID, planner);
    }

    private LinkedList<User> concretizeUsers(List<? extends IUser> users) {
        LinkedList<User> mailto = new LinkedList<>();
        for (IUser iface : users){
            mailto.add(new User(iface));
        }
        return mailto;
    }

    /**
     * @return the basePath
     */
    public String getBasePath() {
        return basePath;
    }

    /**
     * @param basePath the basePath to set
     */
    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    @Override
    public void registerListener(String raportID, IGuiRefreshListener listener) {
        SessionContainerFactory.instance().register(raportID, listener);
    }

    @Override
    public void unregisterListener(String raportID) {
        SessionContainerFactory.instance().unregister(raportID);
    }

    @Override
    public void unregisterListener(IGuiRefreshListener listener) {
        SessionContainerFactory.instance().unregister(listener);
    }
    
}
