/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package scheduler;

import interfaces.reports.IRaportChanged;
import java.util.Map;
import services.SessionContainerFactory;

/**
 *
 * @author marcin
 */
public class Updater implements IRaportChanged{

    @Override
    public void raportChanged() {
        SessionContainerFactory.instance().invokeAll();
    }

    @Override
    public void setKpis(Map<String, Double> newValues) {
        
    }
    
}
