/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package scheduler;

import scheduler.Scheduler;
import interfaces.gui.ISchedulerProxy;
import interfaces.reports.IRaportChanged;

/**
 *
 * @author marcin
 */
public class SchedulerFactory {
    private static Scheduler soleInstance=null;
    private static IRaportChanged raportChanged=null;
    
    public static Scheduler getSchedulerProxy(){
        if (soleInstance==null){
            soleInstance = new Scheduler();
        }
        return soleInstance;
    }
    
    public static IRaportChanged getUpdaterProxy(){
        if (raportChanged==null){
            raportChanged = new Updater();
        }
        return raportChanged;
    }
    
}
