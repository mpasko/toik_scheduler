package scheduler;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.CamelContext;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.log4j.PropertyConfigurator;

import Camel.SchedulerRouteBuilder;
import interfaces.gui.IGuiRefreshListener;
import interfaces.gui.ISchedulerProxy;
import interfaces.gui.IUser;
import interfaces.reports.IRaportChanged;
import java.util.Arrays;
import model.Planner;
import model.Threshold;
import model.User;

public class Main {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		String log4jConfPath = "src/main/resources/log4j.properties";
		PropertyConfigurator.configure(log4jConfPath);
		

    	Scheduler proxy = SchedulerFactory.getSchedulerProxy();
    	CamelContext context = proxy.getCamelContext();  	
    	List<User> userList = new ArrayList<User>();
		List<User> notifyList = new ArrayList<User>();
		User dan = new User();
		User toik = new User();
		toik.setEmail("toikscheduler@gmail.com");
		dan.setEmail("daniel.wojciechowsk@gmail.com");
		userList.add(toik);
		notifyList.add(dan);
		
		Threshold threshold = new Threshold();
		threshold.value= 20.0;
		threshold.kpi = "10";
		Planner planner = new Planner();
		planner.setMailto(notifyList);
		planner.setMailtoPeriodic(userList);
		planner.setScheduleTime(Arrays.asList("0+0/1+12-21+?+*+*"));
		planner.setRaportID("1");
		planner.addThreshold(threshold);
		try {
			context.addRoutes(new SchedulerRouteBuilder(planner));
			context.start();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
