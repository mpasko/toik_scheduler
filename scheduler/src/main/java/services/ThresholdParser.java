/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import interfaces.gui.InvalidOperatorException;
import interfaces.gui.ExpectedKpiAndValueException;
import java.util.LinkedList;
import java.util.List;
import model.Operator;
import model.Threshold;

/**
 *
 * @author marcin
 */
public class ThresholdParser {
    public static Threshold parse(String text) throws Exception{
        Threshold threshold = new Threshold();
        if (text.lastIndexOf("<=")>-1){
            threshold.operator=Operator.LESS_OR_EQUAL;
        }else if (text.lastIndexOf(">=")>-1){
            threshold.operator=Operator.MORE_OR_EQUAL;
        }else if (text.lastIndexOf(">")>-1){
            threshold.operator=Operator.MORE;
        }else if (text.lastIndexOf("<")>-1){
            threshold.operator=Operator.LESS;
        }else if (text.lastIndexOf("=")>-1){
            threshold.operator=Operator.EQUAL;
        }else{
            throw new InvalidOperatorException("Invalid operator in expression: "+text);
        }
        text=text.replaceAll(",", ".");
        text=text.replaceAll("<", "=");
        text=text.replaceAll(">", "=");
        text=text.replaceAll("==", "=");
        text=text.replaceAll(" ", "");
        String[] rows = text.split("=");
        if ((rows.length<2) || rows[0].isEmpty()){
            throw new ExpectedKpiAndValueException("Expected kpi and value in expression: "+text);
        }
        threshold.kpi=rows[0];
        try{
            threshold.value=Double.parseDouble(rows[1]);
        }catch(NumberFormatException caught){
            final ExpectedKpiAndValueException exception = new ExpectedKpiAndValueException("Expected kpi and value in expression: "+text);
            exception.addSuppressed(caught);
            throw exception;
        }
        return threshold;
    }

    public static List<String> thresholdsToString(List<Threshold> thresholds) {
        List<String> result = new LinkedList<String>();
        for (Threshold thr : thresholds){
            result.add(thr.toString());
        }
        return result;
    }
    
    public static List<Threshold> stringsToThresholds(List<String> thresholds) throws Exception{
        List<Threshold> result = new LinkedList<Threshold>();
        for (String thr : thresholds){
            result.add(parse(thr));
        }
        return result;
    }
}
