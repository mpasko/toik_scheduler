/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import model.Planner;
import org.apache.log4j.Logger;

/**
 *
 * @author marcin
 */
public class FilePersistence {
    private final Logger logger;
    
    private String basePath="";
    
    public FilePersistence(){
        this.logger=org.apache.log4j.Logger.getLogger(this.getClass());
    }
    
    public FilePersistence(String basePath){
        this();
        if (!basePath.endsWith("/")){
            this.basePath=basePath+"/";
        }else{
            this.basePath=basePath;
        }
        if (Files.notExists(Paths.get(this.basePath), LinkOption.NOFOLLOW_LINKS)){
            new File(this.basePath).mkdirs();
        }
    }
    
    public void save(String filename, Planner planner) {
        try{
            File file = new File(basePath+filename);
            XmlMapper xmlMapper = new XmlMapper();
            xmlMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
            xmlMapper.configure(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS, false);
            xmlMapper.writeValue(file, planner);
        }catch(IOException ex){
            logger.error(String.format("io exception during save in location:%s", filename), ex);
        }
    }
    
    public Planner load(String filename){
        try{
            File file = new File(basePath+filename);
            XmlMapper xmlMapper = new XmlMapper();
//            xmlMapper.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true);
            xmlMapper.configure(DeserializationFeature.EAGER_DESERIALIZER_FETCH, true);
            return xmlMapper.readValue(file, Planner.class);
        }catch(IOException ex){
            //Create empty when file not exists
            return new Planner();
        }
    }

    public boolean clearFile(String raports) {
        return new File(basePath+raports).delete();
    }
}
