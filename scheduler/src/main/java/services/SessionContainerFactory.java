/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import interfaces.gui.IGuiRefreshListener;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author marcin
 */
public class SessionContainerFactory {
    private static SessionContainerFactory __instance;
    private Map<String,IGuiRefreshListener> sessions;
    
    private SessionContainerFactory(){
        sessions = new HashMap<>();
    }
    
    public static SessionContainerFactory instance(){
        if (__instance==null){
            __instance=new SessionContainerFactory();
        }
        return __instance;
    }
    
    public void register(String raportId, IGuiRefreshListener listener){
        this.sessions.put(raportId, listener);
    }
    
    public void invokeAll(){
        for (String id : sessions.keySet()){
            sessions.get(id).refreshGui(id);
        }
    }

    public void unregister(String raportID) {
        sessions.remove(raportID);
    }

    public void unregister(IGuiRefreshListener listener) {
        IGuiRefreshListener to_remove=null;
        for (String id : sessions.keySet()){
             to_remove = sessions.get(id);
        }
        if(to_remove!=null){
            sessions.remove(to_remove);
        }
    }
}
