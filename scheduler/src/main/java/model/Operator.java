/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author marcin
 */
@XmlRootElement
public enum Operator {
    LESS("<"),EQUAL("="),MORE(">"),LESS_OR_EQUAL("<="),MORE_OR_EQUAL(">=");
    private String op;
    
    Operator(String op){
        this.op = op;
    }
    
    public String toText(){
        return op;
    }
}
