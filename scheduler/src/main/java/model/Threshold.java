/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

/**
 *
 * @author marcin
 */
@JacksonXmlRootElement
public class Threshold{
    @JacksonXmlProperty(isAttribute=false)
    public Double value;
    @JacksonXmlProperty(isAttribute=false)
    public Operator operator;
    @JacksonXmlProperty(isAttribute=false)
    public String kpi;
    
    @Override
    public String toString(){
        return kpi+operator.toText()+value;
    }
    @JsonIgnore
    public String getKpi(){
    	return this.kpi;
    }
    @JsonIgnore
    public Double getValue(){
    	return value;
    }
}
