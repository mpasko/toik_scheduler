/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import interfaces.gui.IUser;

/**
 *
 * @author marcin
 */
@JacksonXmlRootElement
public class User implements IUser{
    private String name;
    private String email;

    public User(){}
    
    public User(IUser iface) {
        this.email = iface.getEmail();
        this.name = iface.getName();
    }

    /**
     * @return the name
     */
    
    @JacksonXmlProperty(isAttribute=false)
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the email
     */
    
    @JacksonXmlProperty(isAttribute=false)
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
    
    @Override
    public String toString(){
        return this.email;
    }
}
