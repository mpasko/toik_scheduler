/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import java.util.LinkedList;
import java.util.List;
/**
 *
 * @author marcin
 */
@JacksonXmlRootElement
public class Planner {
    private String raportID;
    private List<Threshold> thresholds;
    private List<User> mailto;
    private List<User> mailtoPeriodic;
    private String messageThresholded;
    private String messagePeriodic;
    private List<String> schedule;
    
    public Planner(){
        super();
        this.thresholds=new LinkedList<Threshold>();
        this.mailto=new LinkedList<User>();
        this.mailtoPeriodic=new LinkedList<User>();
    }
    /**
     * @return the raportID
     */
    @JacksonXmlProperty(isAttribute=true)
    public String getRaportID() {
        return raportID;
    }

    /**
     * @param raportID the raportID to set
     */
    public void setRaportID(String raportID) {
        this.raportID = raportID;
    }

    /**
     * @return the list of thresholds
     */
    
    @JacksonXmlProperty(isAttribute=false)
    public List<Threshold> getThresholds() {
        return thresholds;
    }

    /**
     * @param thresholds the list of thresholds to set
     */
    public void setThresholds(List<Threshold> thresholds) {
        this.thresholds = thresholds;
    }

    /**
     * @return the mail
     */
    @JacksonXmlProperty(isAttribute=false)
    public List<User> getMailto() {
        return mailto;
    }

    /**
     * @param mailto the mail to set
     */
    public void setMailto(List<User> mailto) {
        this.mailto = mailto;
    }
    
    public void addThreshold(Threshold threshold) {
        this.thresholds.add(threshold);
    }
    
    public void addMailTo(String mail) {
        User u = new User();
        u.setEmail(mail);
        this.mailto.add(u);
    }

    /**
     * @return the schedule
     */
    @JacksonXmlProperty(isAttribute=false)
    public List<String> getScheduleTime() {
        return schedule;
    }

    /**
     * @param schedule the schedule to set
     */
    public void setScheduleTime(List<String> schedule) {
        this.schedule = schedule;
    }

    /**
     * @return the mailtoPeriodic
     */
    @JacksonXmlProperty(isAttribute=false)
    public List<User> getMailtoPeriodic() {
        return mailtoPeriodic;
    }

    /**
     * @param mailtoPeriodic the mailtoPeriodic to set
     */
    public void setMailtoPeriodic(List<User> mailtoPeriodic) {
        this.mailtoPeriodic = mailtoPeriodic;
    }

    /**
     * @return the messageThresholded
     */
    @JacksonXmlProperty(isAttribute=false)
    public String getMessageThresholded() {
        return messageThresholded;
    }

    /**
     * @param messageThresholded the messageThresholded to set
     */
    public void setMessageThresholded(String messageThresholded) {
        this.messageThresholded = messageThresholded;
    }

    /**
     * @return the messagePeriodic
     */
    @JacksonXmlProperty(isAttribute=false)
    public String getMessagePeriodic() {
        return messagePeriodic;
    }

    /**
     * @param messagePeriodic the messagePeriodic to set
     */
    public void setMessagePeriodic(String messagePeriodic) {
        this.messagePeriodic = messagePeriodic;
    }
    
}
