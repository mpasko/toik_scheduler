/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Arrays;
import java.util.List;
import junit.framework.TestCase;

/**
 *
 * @author marcin
 */
public class PlannerTest extends TestCase {
    
    public PlannerTest(String testName) {
        super(testName);
    }

    public void testSetGetMailto() {
        System.out.println("setgetMailto");
        List<User> mailto = Arrays.asList(new User(),new User());
        Planner instance = new Planner();
        instance.setMailto(mailto);
        List result = instance.getMailto();
        assertEquals(mailto, result);
    }

    public void testGetGetMailtoPeriodic() {
        System.out.println("setgetMailtoPeriodic");
        List<User> mailto = Arrays.asList(new User(),new User());
        Planner instance = new Planner();
        instance.setMailtoPeriodic(mailto);
        List result = instance.getMailtoPeriodic();
        assertEquals(mailto, result);
    }
}
