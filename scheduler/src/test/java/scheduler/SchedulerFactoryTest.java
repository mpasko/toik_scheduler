/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package scheduler;

import Camel.AddAttachmentProcessor;
import Camel.NotifyProcessor;
import Camel.SchedulerRouteBuilder;
import interfaces.gui.IGuiRefreshListener;
import interfaces.gui.ISchedulerProxy;
import interfaces.gui.IUser;
import interfaces.reports.IRaportChanged;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import junit.framework.TestCase;
import model.Planner;
import model.Threshold;
import model.User;
import org.apache.camel.CamelContext;
import org.apache.camel.Processor;
import org.apache.camel.builder.ExchangeBuilder;
import static org.mockito.Mockito.*;
import services.FilePersistence;

/**
 *
 * @author marcin
 */
public class SchedulerFactoryTest extends TestCase {
    
    public SchedulerFactoryTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        ((Scheduler)SchedulerFactory.getSchedulerProxy()).setBasePath("test/planners");
        for (int i=1; i<=5; ++i){
            new FilePersistence("test/planners").clearFile(String.format("raportR%s", i));
        }
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of getSchedulerProxy method, of class SchedulerFactory.
     */
    
    <T> List<T> produceList(T ...items){
        return new LinkedList<>(Arrays.asList(items));
    }
            
    public void testGetKpisFromSchedulerProxy() {
        System.out.println("getSchedulerProxy");
        ISchedulerProxy proxy = SchedulerFactory.getSchedulerProxy();
        assertNotSame(proxy, null);
        final String raportID = "R1";
        proxy.setKPIList(raportID, produceList("efficiency","productiveness"));
        List<String> kpis = proxy.getKPIList(raportID);
        assertTrue(kpis.contains("efficiency"));
        assertTrue(kpis.contains("productiveness"));
    }
    
    public void testGetScheduleTimeFromSchedulerProxy() {
        System.out.println("getSchedulerProxy");
        ISchedulerProxy proxy = SchedulerFactory.getSchedulerProxy();
        assertNotSame(proxy, null);
        final String time = "0+0/1+12-21+?+*+MON";
        final String raportID = "R2";
        proxy.setScheduleTime(raportID, Arrays.asList(time));
        List<String> actual = proxy.getScheduleTime(raportID);
        assertEquals(time, actual.get(0));
    }
    
    public void testGetMessageWhenPlannedFromSchedulerProxy() {
        System.out.println("getSchedulerProxy");
        ISchedulerProxy proxy = SchedulerFactory.getSchedulerProxy();
        assertNotSame(proxy, null);
        final String message = "Dear user1,\nscheduled report!";
        final String raportID = "R3";
        proxy.setMessageWhenPlanned(raportID, message);
        String actual = proxy.getMessageWhenPlanned(raportID);
        assertEquals(message, actual);
    }
    
    public void testGetMessageWhenThresholdFromSchedulerProxy() {
        System.out.println("getSchedulerProxy");
        ISchedulerProxy proxy = SchedulerFactory.getSchedulerProxy();
        assertNotSame(proxy, null);
        final String message = "Dear user1,\nthreshold occured!";
        final String raportID = "R4";
        proxy.setMessageWhenThreshold(raportID, message);
        String actual = proxy.getMessageWhenThreshold(raportID);
        assertEquals(message, actual);
    }
    
    public void testGetThresholdSettingsFromSchedulerProxy() {
        try {
            System.out.println("getSchedulerProxy");
            ISchedulerProxy proxy = SchedulerFactory.getSchedulerProxy();
            assertNotSame(proxy, null);
            final String raportID = "R5";
            List<String> settings = produceList("productivity>0.5","effectiveness>=0,4");
            proxy.setThresholdSettings(raportID, settings);
            List<String> actual = proxy.getThresholdSettings(raportID);
            System.out.println(actual);
            assertTrue(actual.contains("productivity>0.5"));
            assertTrue(actual.contains("effectiveness>=0.4"));
        } catch (Exception ex) {
            Logger.getLogger(SchedulerFactoryTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("Error during setting thresholds!");
        }
    }
    
    public void testGetUsersToNotifyFromSchedulerProxy() {
        System.out.println("getSchedulerProxy");
        ISchedulerProxy proxy = SchedulerFactory.getSchedulerProxy();
        assertNotSame(proxy, null);
        IUser user1 = mock(IUser.class);
        when(user1.getEmail()).thenReturn("user1@mail.com");
        when(user1.getName()).thenReturn("User1");
        IUser user2 = mock(IUser.class);
        when(user2.getEmail()).thenReturn("user2@mail.com");
        when(user2.getName()).thenReturn("User2");
        List<IUser> users = produceList(user1,user2);
        final String raportID = "R6";
        proxy.setUsersToNotify(raportID, users);
        List<? extends IUser> actual = proxy.getUsersToNotify(raportID);
        assertEquals(2, actual.size());
        assertEquals("user1@mail.com",actual.get(0).getEmail());
        assertEquals("user2@mail.com",actual.get(1).getEmail());
        assertEquals("User1",actual.get(0).getName());
        assertEquals("User2",actual.get(1).getName());
    }
    
    public void testGetUsersToSendPeriodicFromSchedulerProxy() {
        System.out.println("getSchedulerProxy");
        ISchedulerProxy proxy = SchedulerFactory.getSchedulerProxy();
        assertNotSame(proxy, null);
        IUser user1 = mock(IUser.class);
        when(user1.getEmail()).thenReturn("user1@mail.com");
        when(user1.getName()).thenReturn("User1");
        IUser user2 = mock(IUser.class);
        when(user2.getEmail()).thenReturn("user2@mail.com");
        when(user2.getName()).thenReturn("User2");
        List<IUser> users = produceList(user1,user2);
        final String raportID = "R7";
        proxy.setUsersToSendPeriodic(raportID, users);
        List<? extends IUser> actual = proxy.getUsersToSendPeriodic(raportID);
        assertNotSame(actual, null);
        assertEquals(2, actual.size());
        assertEquals("user1@mail.com",actual.get(0).getEmail());
        assertEquals("user2@mail.com",actual.get(1).getEmail());
        assertEquals("User1",actual.get(0).getName());
        assertEquals("User2",actual.get(1).getName());
    }

    public void testGetUpdaterProxy() {
        System.out.println("getUpdaterProxy");
        ISchedulerProxy proxy = SchedulerFactory.getSchedulerProxy();
        IRaportChanged updater = SchedulerFactory.getUpdaterProxy();
        assertNotSame(null, updater);
        IGuiRefreshListener listener = mock(IGuiRefreshListener.class);
        final String raportId = "RaportX";
        proxy.registerListener(raportId, listener);
        verify(listener,times(0)).refreshGui(raportId);
        updater.raportChanged();
        verify(listener).refreshGui(raportId);
    }
    
    
    public void testStartingRoutesFromSchedulerProxy(){
    	System.out.println("getSchedulerProxy");
    	Scheduler proxy = SchedulerFactory.getSchedulerProxy();
    	assertNotSame(proxy, null);
    	CamelContext context = proxy.getCamelContext();
    	assertNotSame(context, null);    	
    	List<User> userList = new ArrayList<User>();
		List<User> notifyList = new ArrayList<User>();
		User dan = new User();
		User toik = new User();
		toik.setEmail("toikscheduler@gmail.com");
		dan.setEmail("daniel.wojciechowsk@gmail.com");
		userList.add(toik);
		notifyList.add(dan);
		
		Threshold threshold = new Threshold();
		threshold.value= 20.0;
		threshold.kpi = "10";
		Planner planner = new Planner();
		planner.setMailto(notifyList);
		planner.setMailtoPeriodic(userList);
		planner.setScheduleTime(Arrays.asList("0/1+0+12-21+?+*+*"));
		planner.setRaportID("1");
		planner.addThreshold(threshold);
		try {
			SchedulerRouteBuilder routeBuilder = new SchedulerRouteBuilder(planner);
			context.addRoutes(routeBuilder);
			context.start();
			routeBuilder.configure();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail("exception on starting route");
		}
		Processor proc = new NotifyProcessor("/target/subfolder", "1");
		Processor add = new AddAttachmentProcessor("target/subfolder/", "1");
		assertNotSame(proc, null);
		try {
			proc.process(ExchangeBuilder.anExchange(context).build());
			add.process(ExchangeBuilder.anExchange(context).build());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail("exception on starting route");
		}
		//camelContext.addRoutes(new SchedulerRouteBuilder("0+0/1+12-21+?+*+MON" , recipients, "1"));
		
    	
    	
    }
    
}
