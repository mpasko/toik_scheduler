/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import junit.framework.TestCase;
import model.Operator;
import model.Planner;
import model.Threshold;
import model.User;

/**
 *
 * @author marcin
 */
public class FilePersistenceTest extends TestCase {
    
    public FilePersistenceTest(String testName) {
        super(testName);
    }
    
    public static Planner sampleGenerator(){
        Planner planner = new Planner();
        planner.setRaportID("x3m8c4z2b6x34m");
        planner.addMailTo("toikscheduler@gmail.com");
        planner.addMailTo("pasko@student.agh.edu.pl");
        final User user1 = new User();
        user1.setEmail("userY@mail.com");
        planner.setMailtoPeriodic(Arrays.asList(user1));
        try {
            planner.addThreshold(ThresholdParser.parse("overallPerformance>=-4.6"));
            planner.addThreshold(ThresholdParser.parse("overallScore<=40.0"));
        } catch (Exception ex) {
            Logger.getLogger(FilePersistenceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return planner;
    }

    /**
     * Test of save method, of class FilePersistence.
     */
    public void testSaveAndLoad() {
        System.out.println("save");
        String filename = "threshold_parser_test_save.xml";
        Planner planner = sampleGenerator();
        FilePersistence instance = new FilePersistence("test/planners");
        instance.clearFile(filename);
        instance.save(filename, planner);
        assertTrue(Files.exists(Paths.get("test/planners/"+filename), LinkOption.NOFOLLOW_LINKS));
        
        System.out.println("load");
        final String expKpi = "overallPerformance";
        final Object expOperator=Operator.MORE_OR_EQUAL;
        final Double expVal = -4.6;
        final String expMail = "toikscheduler@gmail.com";
        final String expID = "x3m8c4z2b6x34m";
        final String expMailPeriodic = "userY@mail.com";
        Planner result = instance.load(filename);
        assertNotNull(result);
        assertEquals(2, result.getMailto().size());
        assertEquals(1, result.getMailtoPeriodic().size());
        assertEquals(expMail, result.getMailto().get(0).toString());
        assertEquals(expMailPeriodic, result.getMailtoPeriodic().get(0).toString());
        assertEquals(expID, result.getRaportID());
        assertEquals(2, result.getThresholds().size());
        Threshold threshold = result.getThresholds().get(0);
        assertNotNull(threshold);
        assertEquals(expKpi, threshold.kpi);
        assertEquals(expOperator, threshold.operator);
        assertEquals(expVal, threshold.value);
    }

}
