/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import interfaces.gui.ExpectedKpiAndValueException;
import interfaces.gui.InvalidOperatorException;
import junit.framework.Assert;
import junit.framework.TestCase;
import model.Operator;
import model.Threshold;

/**
 *
 * @author marcin
 */
public class ThresholdParserTest extends TestCase {
    
    public ThresholdParserTest(String testName) {
        super(testName);
    }

    /**
     * Test of parse method, of class ThresholdParser.
     */
    public void testMorePlus() throws Exception {
        System.out.println("parse");
        String text = "overallPerformance>3.5";
        String expKpi = "overallPerformance";
        Object expOperator=Operator.MORE;
        Double expVal = 3.5;
        Threshold result = ThresholdParser.parse(text);
        assertEquals(expKpi, result.kpi);
        assertEquals(expOperator, result.operator);
        assertEquals(expVal, result.value, 0.25);
    }
    
    /**
     * Test of parse method, of class ThresholdParser.
     */
    public void testLessEqualMinus() throws Exception {
        System.out.println("parse");
        String text = "overallPerformance<=-122.4444";
        String expKpi = "overallPerformance";
        Object expOperator=Operator.LESS_OR_EQUAL;
        Double expVal = -122.4444;
        Threshold result = ThresholdParser.parse(text);
        assertEquals(expKpi, result.kpi);
        assertEquals(expOperator, result.operator);
        assertEquals(expVal, result.value, 0.001);
    }
    
    /**
     * Test of parse method, of class ThresholdParser.
     */
    public void testEqualPlus() throws Exception {
        System.out.println("parse");
        String text = "overallPerformance=122000000.0";
        String expKpi = "overallPerformance";
        Object expOperator=Operator.EQUAL;
        Double expVal = 122000000.0;
        Threshold result = ThresholdParser.parse(text);
        assertEquals(expKpi, result.kpi);
        assertEquals(expOperator, result.operator);
        assertEquals(expVal, result.value, 0.5);
    }
    
    /**
     * Test of throwing InvalidOperatorException
     */
    public void testWrongOperator() throws Exception {
        System.out.println("parse");
        String text = "overallPerformance$122000000.0";
        try {
            ThresholdParser.parse(text);
            fail("Expected exception but exception not thrown");
        } catch (InvalidOperatorException expected){
            Assert.assertTrue(true);
        } catch (Throwable unexpected){
            fail(String.format("method throws unexpected exception:%s", unexpected));
        }
    }
    
    /**
     * Test of throwing ExpectedKpiAndValueException when kpi missing
     */
    public void testWrongKPI() throws Exception {
        System.out.println("parse");
        String text = "<122000000.0";
        try {
            ThresholdParser.parse(text);
            fail("Expected exception but exception not thrown");
        } catch (ExpectedKpiAndValueException expected){
            Assert.assertTrue(true);
        } catch (Throwable unexpected){
            fail(String.format("method throws unexpected exception:%s", unexpected));
        }
    }
    
    /**
     * Test of throwing ExpectedKpiAndValueException when value missing
     */
    public void testMissingValue() throws Exception {
        System.out.println("parse");
        String text = "performance< ";
        try {
            ThresholdParser.parse(text);
            fail("Expected exception but exception not thrown");
        } catch (ExpectedKpiAndValueException expected){
            Assert.assertTrue(true);
        } catch (Throwable unexpected){
            fail(String.format("method throws unexpected exception:%s", unexpected));
        }
    }
    
    /**
     * Test of throwing ExpectedKpiAndValueException when incorrect value
     */
    public void testWrongValue() throws Exception {
        System.out.println("parse");
        String text = "performance<unparsable double";
        try {
            ThresholdParser.parse(text);
            fail("Expected exception but exception not thrown");
        } catch (ExpectedKpiAndValueException expected){
            Assert.assertTrue(true);
        } catch (Throwable unexpected){
            fail(String.format("method throws unexpected exception:%s", unexpected));
        }
    }
}
